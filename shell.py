async def call_shell(command: str) -> str:
    """Runs a command in the system's shell

    Parameters
    ----------
    command : str
        The command to send to the system shell

    Returns
    -------
    str
        The output of the command executed
    """
    try:
        pipe = asyncio.subprocess.PIPE
        process = await asyncio.create_subprocess_shell(command,
                                                        stdout=pipe,
                                                        stderr=pipe)
        stdout, stderr = await process.communicate()
    except NotImplementedError:
        process = subprocess.Popen(command, shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()

    if stdout and stderr:
        return f"$ {command}\n\n[stderr]\n"\
               f"{stderr.decode('utf-8')}===\n"\
               f"[stdout]\n{stdout.decode('utf-8')}"
    elif stdout:
        return f"$ {command}\n\n"\
               f"[stdout]\n{stdout.decode('utf-8')}"
    elif stderr:
        return f"$ {command}\n\n"\
               f"[stderr]\n{stderr.decode('utf-8')}"