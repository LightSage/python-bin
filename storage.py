"""
MIT License

Copyright (c) 2020 LightSage

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import asyncio
import json


class Storage:
    def __init__(self, file_name: str, *, loop=None):

        self.file_name = file_name
        self.lock = asyncio.Lock()
        self.loop = loop if loop else asyncio.get_event_loop()
        self.load_file()

    def load_file(self):
        """Loads the file"""
        try:
            with open(self.file_name, 'r') as f:
                self._storage = json.load(f)
        except FileNotFoundError:
            self._storage = dict()

    def _dump(self):
        with open(self.file_name, 'w') as fp:
            json.dump(self._storage.copy(), fp, ensure_ascii=True, separators=(',', ':'))

    async def save(self):
        async with self.lock:
            await self.loop.run_in_executor(None, self._dump)

    def get(self, key: str):
        """Gets an entry in storage.

        Args:
            key (str): The key to get from storage
        """
        return self._storage.get(str(key))

    async def add(self, key: str, value):
        """Adds a new entry in the storage and saves.

        Args:
            key (str): The key to add
            value: The value to associate to the key
        """
        self._storage[str(key)] = value
        await self.save()

    async def pop(self, key: str):
        """Pops a storage key and saves.

        Args:
            key (str): The key to pop from storage.

        Returns:
            dict: The value of the key that was popped.
        """
        value = self._storage.pop(str(key))
        await self.save()
        return value

    def __contains__(self, item: str):
        return str(item) in self._storage

    def __getitem__(self, item: str):
        return self._storage[str(item)]

    def __len__(self):
        return len(self._storage)
